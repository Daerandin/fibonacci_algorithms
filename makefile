# fibonacci algorithms: calculating a specific fibonacci number either with a recursive function or through dynamic programming
# these two programs are for educational purposes, and initially made to demonstrate the problems with recursive algorithms for certain purposes

OUT1 = recursive_fibonacci
OUT2 = dynamic_fibonacci
CC = gcc
OBJ1 = recursive_fibonacci.o
OBJ2 = dynamic_fibonacci.o
OBJ2NOGMP = dynamic_nogmp.o
CFLAGS = -march=native -pthread -pedantic -Wall -Werror -Wextra -fstack-protector-strong -O2

all: clean_dynamic $(OUT1) $(OUT2)

nogmp: clean_dynamic $(OUT1) dynamic_nogmp

recursive_fibonacci: $(OBJ1)
	$(CC) $(CFLAGS) -o $(OUT1) $(OBJ1)

recursive_fibonacci.o: recursive_fibonacci.c
	$(CC) $(CFLAGS) -c -o $@ $<

dynamic_fibonacci: $(OBJ2)
	$(CC) $(CFLAGS) -lgmp -o $(OUT2) $(OBJ2)

dynamic_fibonacci.o: dynamic_fibonacci.c
	$(CC) $(CFLAGS) -lgmp -c -o $@ $<

dynamic_nogmp: $(OBJ2NOGMP)
	$(CC) $(CFLAGS) -o $(OUT2) $(OBJ2NOGMP)

dynamic_nogmp.o: dynamic_nogmp.c
	$(CC) $(CFLAGS) -c -o $@ $<

clean:
	$(RM) $(OUT1) $(OBJ1) $(OUT2) $(OBJ2) $(OBJ2NOGMP)

clean_dynamic:
	$(RM) $(OUT2)

.PHONY: clean
