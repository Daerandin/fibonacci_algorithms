NAME
====
fibonacci algorithms - two different algorithms for calculating a specific fibonacci number

SYNOPSIS
========
Usage:

recursive\_fibonacci [INT]

dynamic\_fibonacci [INT]

DEPENDENCIES
============
gmp (gnu multiple precision arithmetic library)

You can compile this program without gmp if so desired. In that case, be sure to use the 'dynamic\_nogmp.c' source file instead of the 'dynamic\_fibonacci.c' source file. If you use the makefile (suggested on Linux), then simply run 'make nogmp' instead of simply 'make'. Be warned that compiling without the gmp library will only make the fibonacci sequence valid up to, and including, fibonacci number 93, after that the program will suffer from integer overflow and the answers you get will be incorrect.

DESCRIPTION
===========
There are two small programs provided here, one that will calculate a given fibonacci number by using a recursive algorithm, and another program which will use a dynamic programming algorithm. The main purpose of this program is simply to illustrate how quickly the recursive algorithm becomes inadequate for this particular problem. You run the program and provide the desired fibonacci number as an argument to the program. So if you wish to use the dynamic algorithm to find fibonacci number 46, then you simply run 'dynamic\_fibonacci 46' and it will output the answer.

The 'recursive\_fibonacci' program begin to suffer from very long runtimes already at fibonacci number 60. For that purpose, it is not made to be accurate past fibonacci number 93, as it is highly unlikely that anyone have the patience to wait for it to complete running.

The 'dynamic\_fibonacci' program will be able to calculate very high fibonacci numbers (n > 1000000) without taking too long time. If you have the gmp library installed and compiled with it (the default in the makefile), then you will get accurate results as long as your computer have enough memory to store the large numbers. If you compiled without the gmp library, then it is only accurate up to fibonacci number 93, after which integer overflows will occur.

The integer you provide when you run this program has a certain limitation. It can only be a whole number, and it must only contain actual digits. Any other symbols will cause the program to exit with an error. Furthermore, this input number is limited by the size of a long int on your system. Most likely this will be a 64-bit number, but on a 32-bit system it might instead be a 32-bit number. So this program simply can't calculate fibonacci numbers above that limit.

AUTHOR
======
Daniel Jenssen <daerandin@gmail.com>

