/*********************************************************************************************************
 *                                                                                                       *
 * dynamic_fibonacci  - calculate a specific fibonacci number with a dynamic programming algorithm.      *
 *                                                                                                       *
 * Copyright (C) 2019 Daniel Jenssen <daerandin@gmail.com>                                               *
 *                                                                                                       *
 * This program is free software: you can redistribute it and/or modify                                  *
 * it under the terms of the GNU General Public License as published by                                  *
 * the Free Software Foundation, either version 3 of the License, or                                     *
 * (at your option) any later version.                                                                   *
 *                                                                                                       *
 * This program is distributed in the hope that it will be useful,                                       *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of                                        *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                                         *
 * GNU General Public License for more details.                                                          *
 *                                                                                                       *
 * You should have received a copy of the GNU General Public License                                     *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.                                 *
 *                                                                                                       *
 *                                                                                                       *
 * Run this program with an integer as argument                                                          *
 *                                                                                                       *
 *                                                                                                       *
 * Example: dynamic_fibonacci 28                                                                         *
 *                                                                                                       *
 *********************************************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <gmp.h>

char *calc_fibo(long, char **);

int main(int argc, char **argv)
{
    char *ptr;
    if (argc != 2) {
        printf("Usage: %s [INT]\n", *argv);
        exit(EXIT_FAILURE);
    }
    ptr = *(++argv);
    while (*ptr != '\0')
        if (!isdigit(*(ptr++))) {
            printf("Error: argument is not a positive integer\n");
            exit(EXIT_FAILURE);
        }
    ptr = NULL;
    printf("%s\n", calc_fibo(atol(*argv), &ptr));
    if (ptr)
        free(ptr);
    return 0;
}

char *calc_fibo(long n, char **output)
{
    long i;
    mpz_t previous, current;
    mpz_inits(previous, current, NULL);
    mpz_set_si(current, 0);
    mpz_set_si(previous, 1);
    for (i = 0; i < n; i++) {
        mpz_swap(previous, current);
        mpz_add(current, current, previous);
    }
    *output = mpz_get_str(*output, 10, current);
    mpz_clears(previous, current, NULL);
    return *output;
}
