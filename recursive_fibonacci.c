/*********************************************************************************************************
 *                                                                                                       *
 * recursive_fibonacci - calculate a specific fibonacci number with a recursive algorithm.               *
 *                                                                                                       *
 * Copyright (C) 2019 Daniel Jenssen <daerandin@gmail.com>                                               *
 *                                                                                                       *
 * This program is free software: you can redistribute it and/or modify                                  *
 * it under the terms of the GNU General Public License as published by                                  *
 * the Free Software Foundation, either version 3 of the License, or                                     *
 * (at your option) any later version.                                                                   *
 *                                                                                                       *
 * This program is distributed in the hope that it will be useful,                                       *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of                                        *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                                         *
 * GNU General Public License for more details.                                                          *
 *                                                                                                       *
 * You should have received a copy of the GNU General Public License                                     *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.                                 *
 *                                                                                                       *
 *                                                                                                       *
 * Run this program with an integer as argument                                                          *
 *                                                                                                       *
 *                                                                                                       *
 * Example: recursive_fibonacci 28                                                                       *
 *                                                                                                       *
 *********************************************************************************************************/


#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>

unsigned long long calc_fibo(long);

int main(int argc, char **argv)
{
    char *ptr;
    if (argc != 2) {
        printf("Usage: %s [INT]\n", *argv);
        exit(EXIT_FAILURE);
    }
    ptr = *(++argv);
    while (*ptr != '\0')
        if (!isdigit(*(ptr++))) {
            printf("Error: argument is not a positive integer\n");
            exit(EXIT_FAILURE);
        }
    printf("%llu\n", calc_fibo(atol(*argv)));
    return 0;
}

unsigned long long calc_fibo(long n)
{
    return (n < 3 ? (n ? 1llu : 0llu) : calc_fibo(n-1) + calc_fibo(n-2));
}
